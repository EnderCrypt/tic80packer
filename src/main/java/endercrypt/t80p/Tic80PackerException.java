package endercrypt.t80p;

@SuppressWarnings("serial")
public class Tic80PackerException extends RuntimeException
{
	public Tic80PackerException(String message)
	{
		super(message);
	}
	
	public Tic80PackerException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
