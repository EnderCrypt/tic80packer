package endercrypt.t80p;


import endercrypt.library.commons.JarInfo;
import endercrypt.library.groundwork.command.StandardArguments;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;


@Getter
public class Arguments extends StandardArguments
{
	@Option(names = { "--rom" }, required = true)
	private Path romPath;
	
	public Path getRomPath()
	{
		return JarInfo.workingDirectory.resolve(romPath);
	}
	
	@Option(names = { "-o", "--output" }, defaultValue = "-")
	private String output;
	
	@Parameters(arity = "1..*", paramLabel = "SOURCES")
	private List<Path> paths = new ArrayList<>();
}
