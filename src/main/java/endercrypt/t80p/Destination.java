package endercrypt.t80p;


import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;


public interface Destination
{
	public static Destination create(String name)
	{
		if (name.strip().equals("-"))
		{
			return DestinationStdout.instance;
		}
		
		return new DestinationFile(Path.of(name));
	}
	
	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class DestinationStdout implements Destination
	{
		public static final Destination instance = new DestinationStdout();
		
		@Override
		public PrintStream createStream()
		{
			return System.out;
		}
	}
	
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class DestinationFile implements Destination
	{
		private final Path path;
		
		@Override
		public PrintStream createStream() throws FileNotFoundException
		{
			return new PrintStream(new BufferedOutputStream(new FileOutputStream(path.toFile())));
		}
	}
	
	public PrintStream createStream() throws IOException;
}
