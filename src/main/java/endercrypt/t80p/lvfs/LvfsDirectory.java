package endercrypt.t80p.lvfs;


import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;


@Getter
public class LvfsDirectory extends LvfsEntry
{
	public static String createLuaFsPath(Path path)
	{
		return "fs.root" + createTablePath(path);
	}
	
	private static String createTablePath(Path path)
	{
		if (path == null)
		{
			return "";
		}
		List<String> directorySegments = List.of(path.toString().split("/"));
		String firstSegment = directorySegments.stream().findFirst().orElse("");
		if (firstSegment.equals(""))
		{
			return "";
		}
		return directorySegments.stream()
			.map(LvfsEntry::createLuaFsSegment)
			.collect(Collectors.joining(""));
	}
	
	private final LvfsCollection<LvfsDirectory> directories = new LvfsCollection<>(this, LvfsDirectory::new);
	private final LvfsCollection<LvfsFile> files = new LvfsCollection<>(this, LvfsFile::new);
	
	public LvfsDirectory(LvfsDirectory parent, Path path)
	{
		super(parent, path);
		if (Files.isDirectory(path) == false)
		{
			throw new IllegalArgumentException(path + " is not a directory");
		}
	}
	
	public void add(Path path)
	{
		Path relative = getPath().relativize(path);
		String segment = relative.subpath(0, 1).toString();
		if (segment.equals(""))
		{
			return;
		}
		if (segment.equals(".."))
		{
			throw new IllegalArgumentException("Cant add " + path + " as its outside this directory");
		}
		Path target = getPath().resolve(segment);
		if (Files.isRegularFile(target))
		{
			files.put(new LvfsFile(this, target));
			return;
		}
		if (Files.isDirectory(target))
		{
			directories.retrieve(segment).add(path);
			return;
		}
		throw new IllegalArgumentException(target + " is an unknown filesystem entity");
	}
	
	@Override
	public void write(PrintStream stream) throws IOException
	{
		stream.println(createLuaFsPath(getRelativePath()) + " = {}");
		for (LvfsDirectory directory : directories)
		{
			directory.write(stream);
		}
		for (LvfsFile file : files)
		{
			file.write(stream);
		}
	}
}
