package endercrypt.t80p.lvfs;


import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class LvfsCollection<T extends LvfsEntry> implements Iterable<T>
{
	private final Map<String, T> entries = new HashMap<>();
	
	private final LvfsDirectory owner;
	private final Constructor<T> constructor;
	
	public Optional<T> get(String name)
	{
		return Optional.ofNullable(entries.get(name));
	}
	
	protected T retrieve(String name)
	{
		return entries.computeIfAbsent(name, this::create);
	}
	
	protected void put(T entry)
	{
		String name = entry.getName();
		if (entries.containsKey(name))
		{
			throw new IllegalArgumentException("There already is an entry at " + name);
		}
		entries.put(name, entry);
	}
	
	private T create(String name)
	{
		LvfsDirectory parent = owner.getParent();
		Path path = owner.getPath().resolve(name);
		return constructor.create(parent, path);
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return entries.values().iterator();
	}
	
	@Override
	public String toString()
	{
		return entries.values().toString();
	}
	
	public interface Constructor<T extends LvfsEntry>
	{
		public T create(LvfsDirectory parent, Path path);
	}
}
