package endercrypt.t80p.lvfs;


import endercrypt.library.commons.misc.Ender;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;


public class LvfsFile extends LvfsEntry
{
	public LvfsFile(LvfsDirectory parent, Path path)
	{
		super(parent, path);
		if (Files.isRegularFile(path) == false)
		{
			throw new IllegalArgumentException(path + " is not a file");
		}
	}
	
	public String createBinaryString() throws IOException
	{
		int estimatedFileSize = (int) (Files.size(getPath()) + 128);
		StringBuilder characters = new StringBuilder(estimatedFileSize);
		try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(getPath().toFile())))
		{
			int b;
			while ((b = input.read()) != -1)
			{
				if (isSpecialCharacter((byte) b))
				{
					String escapeCodepoint = Ender.text.padLeft(String.valueOf(b), '0', 3);
					characters.append("\\" + escapeCodepoint);
				}
				else
				{
					characters.append((char) b);
				}
			}
		}
		return "\"" + characters.toString() + "\"";
	}
	
	private static boolean isSpecialCharacter(byte b)
	{
		if (b < ' ')
			return true;
		if (b == '"')
			return true;
		if (b == '\\')
			return true;
		if (b > '~')
			return true;
		return false;
	}
	
	@Override
	public void write(PrintStream stream) throws IOException
	{
		String luaFs = LvfsDirectory.createLuaFsPath(getRelativePath().getParent());
		String filename = getPath().getFileName().toString();
		stream.println(luaFs + LvfsEntry.createLuaFsSegment(filename) + " = " + createBinaryString());
	}
}
