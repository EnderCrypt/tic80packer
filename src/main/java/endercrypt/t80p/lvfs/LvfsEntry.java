package endercrypt.t80p.lvfs;


import endercrypt.library.commons.JarInfo;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


/**
 * Lua virtual file system
 */
@RequiredArgsConstructor
@Getter
public abstract class LvfsEntry
{
	public static String createLuaFsSegment(String filename)
	{
		return "[\"" + filename + "\"]";
	}
	
	private final LvfsDirectory parent;
	private final Path path;
	
	public Path getRelativePath()
	{
		return JarInfo.workingDirectory.relativize(getPath());
	}
	
	public String getName()
	{
		return path.getFileName().toString();
	}
	
	public abstract void write(PrintStream stream) throws IOException;
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "[name=" + getName() + "]";
	}
}
