package endercrypt.t80p.lvfs;


import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;


public class LvfsRoot extends LvfsDirectory
{
	public LvfsRoot(Path path)
	{
		super(null, path);
	}
	
	@Override
	public void write(PrintStream stream) throws IOException
	{
		stream.println("fs = {}");
		super.write(stream);
	}
}
