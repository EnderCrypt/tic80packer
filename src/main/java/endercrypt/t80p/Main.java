package endercrypt.t80p;


import endercrypt.library.commons.JarInfo;
import endercrypt.library.commons.data.DataSource;
import endercrypt.library.groundwork.information.Information;
import endercrypt.library.groundwork.launcher.PlainApplication;
import endercrypt.t80p.lvfs.LvfsRoot;
import endercrypt.t80p.rom.Tic80Rom;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;


public class Main extends PlainApplication<Arguments>
{
	public static void main(String[] args)
	{
		PlainApplication.launch(Main.class, args);
	}
	
	@Override
	public void main(Arguments arguments, Information information) throws Exception
	{
		Path romPath = arguments.getRomPath();
		Tic80Rom rom = Tic80Rom.parse(romPath);
		
		LvfsRoot lvfsRoot = new LvfsRoot(JarInfo.workingDirectory);
		arguments.getPaths()
			.stream()
			.flatMap(this::createFilePaths)
			.filter(p -> p.equals(romPath) == false)
			.map(JarInfo.workingDirectory::resolve)
			.forEach(lvfsRoot::add);
		
		Destination destination = Destination.create(arguments.getOutput());
		try (PrintStream stream = destination.createStream())
		{
			stream.println(rom.getHeader());
			lvfsRoot.write(stream);
			stream.println(DataSource.RESOURCES.read("headers/lua.txt").asString());
			stream.print(rom.getCode());
			stream.print(rom.getFooter());
		}
	}
	
	private Stream<Path> createFilePaths(Path path)
	{
		if (Files.isRegularFile(path))
		{
			return Stream.of(path);
		}
		if (Files.isDirectory(path))
		{
			try
			{
				return Files.walk(path)
					.filter(Files::isRegularFile);
			}
			catch (IOException e)
			{
				throw new Tic80PackerException("Failed to walk " + path, e);
			}
		}
		throw new IllegalArgumentException("Unknown filesystem entity: " + path);
	}
}
