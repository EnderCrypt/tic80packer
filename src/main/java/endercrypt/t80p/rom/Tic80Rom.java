package endercrypt.t80p.rom;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.Scanner;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class Tic80Rom
{
	public static Tic80Rom parse(Path path) throws FileNotFoundException
	{
		Tic80RomParser parser = new Tic80RomParser();
		
		try (Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream(path.toFile()))))
		{
			while (scanner.hasNext())
			{
				String line = scanner.nextLine();
				parser.feed(line);
			}
		}
		return parser.build();
	}
	
	private final String header;
	private final String code;
	private final String footer;
	
	@Override
	public String toString()
	{
		return header + code + footer;
	}
}
