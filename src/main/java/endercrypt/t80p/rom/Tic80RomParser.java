package endercrypt.t80p.rom;

public class Tic80RomParser
{
	private final HeaderSection header = new HeaderSection();
	private final CodeSection code = new CodeSection();
	private final FooterSection footer = new FooterSection();
	
	private Section current = header;
	
	public void feed(String line)
	{
		current.parse(line);
	}
	
	private void redirect(String line, Section section)
	{
		current = section;
		feed(line);
	}
	
	public Tic80Rom build()
	{
		return new Tic80Rom(
			header.build(),
			code.build(),
			footer.build());
	}
	
	private class HeaderSection extends Section
	{
		@Override
		public void parse(String line)
		{
			if (line.startsWith("--") == false)
			{
				redirect(line, code);
				return;
			}
			result.append(line).append("\n");
		}
	}
	
	private class CodeSection extends Section
	{
		@Override
		public void parse(String line)
		{
			if (line.startsWith("-- <TILES>"))
			{
				redirect(line, footer);
				return;
			}
			result.append(line).append("\n");
		}
	}
	
	private class FooterSection extends Section
	{
		@Override
		public void parse(String line)
		{
			result.append(line).append("\n");
		}
	}
	
	private static abstract class Section
	{
		protected final StringBuilder result = new StringBuilder();
		
		public abstract void parse(String line);
		
		public final String build()
		{
			return result.toString();
		}
	}
}
