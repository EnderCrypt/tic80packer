function fs.read(path)
	local target = fs.root
	for segment in string.gmatch(path, "([^/]+)") do
		local next = target[segment]
		if next == nil then
			return nil
		end
		target = next
	end
	return target
end

libraries = {}
function require(path)
	if libraries[path] == nil then
		local code = fs.read(path)
		if code == nil then
			error("Missing library "..path)
		end
		
		local chunk, err = load(code, path, nil, _G)
		if err then
			error(err)
		end
		local status, data = pcall(chunk)
		if status == false then
			error("Failed to initialize library "..path.." due to\n"..data)
		end	
		libraries[path] = true
	end
end