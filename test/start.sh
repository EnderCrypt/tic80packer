#!/usr/bin/env bash

set -e

mvn package --file=..

java -jar ../target/Tic80Packer.jar	\
	--rom=roms/input.lua \
	--output=roms/output.lua \
	extra main.lua

tic-80 --cmd "load $PWD/roms/output.lua"
