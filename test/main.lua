local lib = require("extra/lib.lua")

if _G["TIC"] == nil then
	error("TIC() is missing")
end

if data ~= nil then
	error("data was set")
end

local result = lib()
if result ~= 5 then
	error("expected result to be 5, but it was "..tostring(result))
end

trace("all works!", 11) -- green
exit()
